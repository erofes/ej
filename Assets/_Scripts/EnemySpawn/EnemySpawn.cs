﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    #region Singleton Init
    private static EnemySpawn _instance;

    void Awake() // Init in order
    {
        if (_instance == null)
            Init();
        else if (_instance != this)
        {
            Debug.Log($"Destroying {gameObject.name}, caused by one singleton instance");
            Destroy(gameObject);
        }
    }

    public static EnemySpawn Instance // Init not in order
    {
        get
        {
            if (_instance == null)
                Init();
            return _instance;
        }
        private set => _instance = value;
    }

    static void Init() // Init script
    {
        _instance = FindObjectOfType<EnemySpawn>();
        if (_instance != null)
            _instance.Initialize();
    }
    #endregion

    public Transform m_spawnPoint;
    public Transform m_enemyContainer;
    public List<GameObject> m_pool;
    public float m_spawnSpread = 1f;
    public float m_spawnDuration;

    // Internal state
    List<float> m_timerToSpawn;
    List<int> m_enemyCount;

    void Initialize()
    {
        m_timerToSpawn = new List<float>();
        m_enemyCount = new List<int>();
        // Init data here
        enabled = false;
    }

    public void BeginSpawning(float duration)
    {
        m_spawnDuration = duration;
        m_timerToSpawn.Clear();
        m_enemyCount.Clear();
        foreach (var item in GameManager.Instance.CurrentWave.m_waveData.waveEnemies)
        {
            m_timerToSpawn.Add(Time.time);
            m_enemyCount.Add(item.m_count);
        }

        enabled = true;
    }

    public void PoolAllAndStop()
    {
        EnemiesController.Instance.PoolAll();
        enabled = false;
    }

    private void Update()
    {
        m_spawnDuration -= Time.deltaTime;
        for (int i = 0; i < GameManager.Instance.CurrentWave.m_waveData.waveEnemies.Length; i++)
        {
            if (Time.time >= m_timerToSpawn[i] && m_enemyCount[i] > 0)
            {
                m_timerToSpawn[i] = Time.time + GameManager.Instance.CurrentWave.m_waveData.waveEnemies[i].m_spawnRate;
                SpawnEnemy(GameManager.Instance.CurrentWave.m_waveData.waveEnemies[i].m_enemySO);
                m_enemyCount[i]--;
            }
            if (GameManager.Instance.m_waveID != GameManager.Instance.m_maxWave - 1)
                UIManager.Instance.m_waveTimerText.Value.text = $"Next wave in: {m_spawnDuration:0.0}";
        }

        if (m_spawnDuration < 0f)
        {
            enabled = false;
            GameManager.Instance.NextWave(false);
            if (GameManager.Instance.m_waveID == GameManager.Instance.m_maxWave - 1)
                UIManager.Instance.m_waveTimerText.Value.text = $"No waves";
        }
    }

    public bool IsSpawnCompleted()
    {
        bool isComplete = true;
        foreach(var item in m_enemyCount)
        {
            if (item > 0)
                isComplete = false;
        }
        return isComplete;
    }

    public void SpawnEnemy(EnemySO enemySO)
    {
        GameObject enemy = InstantiateOrGetFromPool();
        enemy.GetComponent<SpriteRenderer>().sprite = enemySO.m_sprite;
        enemy.transform.localScale = Vector3.one * enemySO.m_enemySize;
        enemy.GetComponent<EnemyScript>().InitFromData(enemySO.m_enemyData);
        EnemiesController.Instance.Register(enemy.GetComponent<EnemyScript>());
    }

    public GameObject InstantiateOrGetFromPool()
    {
        if (m_pool.Count > 0)
        {
            GameObject go = m_pool[0];
            go.SetActive(true);
            m_pool.RemoveAt(0);
            go.transform.position = m_spawnPoint.position + m_spawnSpread * new Vector3(1f, 1f, 0f);
            return go;
        }
        else
            return Instantiate(GameManager.Instance.m_enemyPrefab, m_spawnPoint.position + m_spawnSpread * new Vector3(1f, 1f, 0f), Quaternion.identity, m_enemyContainer);
    }

    public void Pool(GameObject go)
    {
        go.SetActive(false);
        m_pool.Add(go);
    }
}
