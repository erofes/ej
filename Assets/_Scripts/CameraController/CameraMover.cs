﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class CameraMover : MonoBehaviour
{
    #region Singleton Init
    private static CameraMover _instance;

    void Awake() // Init in order
    {
        if (_instance == null)
            Init();
        else if (_instance != this)
        {
            Debug.Log($"Destroying {gameObject.name}, caused by one singleton instance");
            Destroy(gameObject);
        }
    }

    public static CameraMover Instance // Init not in order
    {
        get
        {
            if (_instance == null)
                Init();
            return _instance;
        }
        private set => _instance = value;
    }

    static void Init() // Init script
    {
        _instance = FindObjectOfType<CameraMover>();
        if (_instance != null)
            _instance.Initialize();
    }
    #endregion

    public bool m_isInverse;
    public float m_sensitivity = 1f;
    public Transform m_cameraTr, m_pointTr;
    public Vector3 m_pointInitPosition;
    public Vector3 m_mouseInitPosition;
    public bool m_isDrag;
    public Vector2 m_boundsX, m_boundsY;
    public Vector2 m_zoomBounds;
    public Vector3 m_targetPos;
    public float m_zoomSpeed = 100f;
    public float m_distance = 10f;
    public float m_lerpT = 0.2f;
    [HideInInspector]
    public Camera m_mainCamera;

    void Initialize()
    {
        m_mainCamera = Camera.main;
        m_targetPos = m_cameraTr.position;
        // Init data here
        enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (FingerGesture.Instance.m_isReadingGestureTwoFinger)
        {
            m_distance = Mathf.Clamp(m_distance + FingerGesture.Instance.m_deltaPercent * m_zoomSpeed * Time.deltaTime, m_zoomBounds.x, m_zoomBounds.y);
            m_targetPos = -m_distance * m_cameraTr.forward + m_pointTr.position;
            m_isDrag = false;
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            m_pointInitPosition = m_pointTr.position;
            m_mouseInitPosition = Input.mousePosition;
            m_isDrag = true;
        }

        if (Input.GetMouseButtonUp(0))
            m_isDrag = false;

        if (m_isDrag)
        {
            Vector2 delta = (m_isInverse) ? Input.mousePosition - m_mouseInitPosition : m_mouseInitPosition - Input.mousePosition;
            m_pointTr.position = m_pointInitPosition
                - delta.x * m_pointTr.right * 0.01f * m_sensitivity
                - delta.y * m_pointTr.up * 0.01f * m_sensitivity;
            m_pointTr.position = new Vector3(
                Mathf.Clamp(m_pointTr.position.x, m_boundsX.x, m_boundsX.y),
                Mathf.Clamp(m_pointTr.position.y, m_boundsY.x, m_boundsY.y),
                m_pointTr.position.z
            );

            m_targetPos = -m_distance * m_cameraTr.forward + m_pointTr.position;
        }

        m_cameraTr.position = Vector3.Lerp(m_cameraTr.position, m_targetPos, m_lerpT);
    }
}
