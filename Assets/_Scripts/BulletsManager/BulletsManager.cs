﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletsManager : MonoBehaviour
{
    #region Singleton Init
    private static BulletsManager _instance;

    void Awake() // Init in order
    {
        if (_instance == null)
            Init();
        else if (_instance != this)
        {
            Debug.Log($"Destroying {gameObject.name}, caused by one singleton instance");
            Destroy(gameObject);
        }
    }

    public static BulletsManager Instance // Init not in order
    {
        get
        {
            if (_instance == null)
                Init();
            return _instance;
        }
        private set => _instance = value;
    }

    static void Init() // Init script
    {
        _instance = FindObjectOfType<BulletsManager>();
        if (_instance != null)
            _instance.Initialize();
    }
    #endregion

    public List<BulletScript> m_spawnedBullets, m_pool;
    public List<Sprite> m_bulletSprite; // By type of tower
    public Transform m_bulletsContainerTr;
    public float m_distanceToHit = 0.5f;

    void Initialize()
    {
        // Init data here
        enabled = true;
    }

    private void FixedUpdate()
    {
        foreach (var item in m_spawnedBullets.ToArray())
        {
            float distanceToTarget = float.PositiveInfinity;
            if (item.m_targetEnemy != null && !item.m_targetEnemy.gameObject.activeInHierarchy)
                item.m_targetEnemy = null; // This one is pooled

            if (item.m_targetEnemy != null)
            {
                distanceToTarget = Vector3.Distance(item.transform.position, item.m_targetEnemy.transform.position);
                item.m_targetPos = item.m_targetEnemy.transform.position;
            }
            else
                distanceToTarget = Vector3.Distance(item.transform.position, item.m_targetPos);

            if (distanceToTarget < m_distanceToHit)
            {
                // Reached target - should attack
                if (item.m_targetEnemy != null)
                {
                    item.m_targetEnemy.ReciveDamage(item.m_damage);
                    if (m_spawnedBullets.Contains(item)) // Clever check on end of game to not pool same bullet
                        Pool(item);
                }
                else if (item.m_splashRange > 0)
                {
                    // Splash hit
                    foreach (var enemy in EnemiesController.Instance.m_enemyScripts.ToArray())
                    {
                        if (Vector2.Distance(item.transform.position, enemy.transform.position) < item.m_splashRange)
                            enemy.ReciveDamage(item.m_damage);
                    }
                    if (m_spawnedBullets.Contains(item)) // Clever check on end of game to not pool same bullet
                        Pool(item);
                }
                else // We dont have enemy at reach, so let's check if we grab one
                {
                    EnemyScript targetEnemy = null;
                    foreach (var enemy in EnemiesController.Instance.m_enemyScripts)
                    {
                        if (Vector2.Distance(item.transform.position, enemy.transform.position) < m_distanceToHit)
                        {
                            if (targetEnemy == null)
                                targetEnemy = enemy;
                            else if (enemy.m_pathPassed > targetEnemy.m_pathPassed)
                                targetEnemy = enemy;
                        }
                    }
                    if (targetEnemy != null)
                        targetEnemy.ReciveDamage(item.m_damage);
                    if (m_spawnedBullets.Contains(item)) // Clever check on end of game to not pool same bullet
                        Pool(item);
                }
            }
            else
            {
                // Still need to fly
                item.transform.position = Vector3.MoveTowards(item.transform.position, item.m_targetPos, Time.deltaTime * item.m_speed);
                if (item.m_isSoldier)
                    item.transform.localRotation = Quaternion.Euler(90f, 90f, 0f);
                else
                    item.transform.LookAt(item.m_targetPos, Vector3.right);
            }
        }
    }

    public void CreateBullet(Vector3 startPos, float damage, float speed, float splashRange, Vector3 targetPos, Sprite sprite, float spriteScale, bool isSoldier, EnemyScript targetEnemy = null)
    {
        BulletScript bulletScript = InstantiateOrGetFromPool(startPos);
        bulletScript.m_damage = damage;
        bulletScript.m_speed = speed;
        bulletScript.m_splashRange = splashRange;
        bulletScript.m_targetPos = targetPos;
        bulletScript.m_targetEnemy = targetEnemy;
        bulletScript.GetComponentInChildren<SpriteRenderer>().sprite = sprite;
        bulletScript.transform.GetChild(0).localScale = Vector3.one * spriteScale;
        bulletScript.m_isSoldier = isSoldier;
        m_spawnedBullets.Add(bulletScript);
    }

    public BulletScript InstantiateOrGetFromPool(Vector3 pos)
    {
        if (m_pool.Count > 0)
        {
            BulletScript bs = m_pool[0];
            bs.gameObject.SetActive(true);
            bs.transform.position = pos;
            m_pool.Remove(bs);
            return bs;
        }
        else
            return Instantiate(GameManager.Instance.m_bulletPrefab, pos, Quaternion.identity, m_bulletsContainerTr).GetComponent<BulletScript>();
    }

    void Pool(BulletScript bulletScript)
    {
        bulletScript.gameObject.SetActive(false);
        m_pool.Add(bulletScript);
        m_spawnedBullets.Remove(bulletScript);
    }

    public void PoolAll()
    {
        foreach (var item in m_spawnedBullets.ToArray())
            Pool(item);
        m_spawnedBullets.Clear();
    }
}
