﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Singleton Init
    private static GameManager _instance;

    void Awake() // Init in order
    {
        if (_instance == null)
            Init();
        else if (_instance != this)
        {
            Debug.Log($"Destroying {gameObject.name}, caused by one singleton instance");
            Destroy(gameObject);
        }
    }

    public static GameManager Instance // Init not in order
    {
        get
        {
            if (_instance == null)
                Init();
            return _instance;
        }
        private set => _instance = value;
    }

    static void Init() // Init script
    {
        _instance = FindObjectOfType<GameManager>();
        if (_instance != null)
            _instance.Initialize();
    }
    #endregion

    [SerializeField]
    int m_money;
    public int Money
    {
        get
        {
            return m_money;
        }
        set
        {
            m_money = value;
            UpdatePlayerStats();
        }
    }
    [SerializeField]
    int m_health;
    public int Health
    {
        get
        {
            return m_health;
        }
        set
        {
            m_health = value;
            if (m_health <= 0)
                OnDefeat();
            UpdatePlayerStats();
        }
    }
    public int m_maxWave;

    [Header("Level")]
    public int m_levelID;
    [System.Serializable]
    public class LevelData
    {
        public int m_levelStartMoney;
        public GameObject m_levelPrefab;
        public List<WaveSO> m_waves;
    }
    public List<LevelData> m_levelData;
    public LevelScript m_currentLevel;
    public Transform m_currentLevelTr;

    [Header("Prefabs")]
    public GameObject m_enemyPrefab;
    public GameObject m_bulletPrefab;

    [Header("Tower Database")]
    public Dictionary<TowerTypeEnum, List<TowerSO>> m_typeToTowerSODict; // For easier access
    public List<TowerSO> m_arrowTower;
    public List<TowerSO> m_splashTower;
    public List<TowerSO> m_freezeTower;
    public List<TowerSO> m_armyTower;
    public int m_waveID;
    public int WaveID { get { return m_waveID; } set { m_waveID = Mathf.Clamp(value, 0, m_levelData[m_levelID].m_waves.Count - 1); } }
    public WaveSO CurrentWave => m_levelData[m_levelID].m_waves[m_waveID];

    [Header("Controlls")]
    public int m_desiredLevel;

    void Initialize()
    {
        m_typeToTowerSODict = new Dictionary<TowerTypeEnum, List<TowerSO>>();
        m_typeToTowerSODict.Add(TowerTypeEnum.FastArrow, m_arrowTower);
        m_typeToTowerSODict.Add(TowerTypeEnum.Splash, m_splashTower);
        m_typeToTowerSODict.Add(TowerTypeEnum.Freeze, m_freezeTower);
        m_typeToTowerSODict.Add(TowerTypeEnum.Army, m_armyTower);

        LoadLevel(0);
        // Init data here
        enabled = true;
    }

    public void LoadLevel(int level)
    {
        Health = 100;
        m_levelID = Mathf.Clamp(level, 0, m_levelData.Count - 1);
        if (m_currentLevel != null)
            Destroy(m_currentLevel.gameObject);
        m_currentLevel = Instantiate(m_levelData[m_levelID].m_levelPrefab).GetComponent<LevelScript>();
        m_currentLevelTr = m_currentLevel.transform;
        StartWaveManager.Instance.ActivateStartWaveButton();
        WaveID = 0; // Start from first wave in new level
        m_maxWave = m_levelData[m_levelID].m_waves.Count;
        Money = m_levelData[m_levelID].m_levelStartMoney;

        UIManager.Instance.m_defeatUI.Value.SetActive(false);
        EnemySpawn.Instance.PoolAllAndStop();
        BulletsManager.Instance.PoolAll();
        BulletsManager.Instance.enabled = true;
        EnemiesController.Instance.enabled = true;
        CameraMover.Instance.enabled = true;
        UIManager.Instance.m_waveTimerText.Value.text = $"Prepare";
    }

    [NaughtyAttributes.Button]
    void LoadLevelManually()
    {
        WaveID = m_desiredLevel;
        LoadLevel(WaveID);
    }

    public void NextLevel()
    {
        int targetLevel = m_levelID + 1;
        if (targetLevel >= m_levelData.Count)
            targetLevel = 0; // Repeat level
        LoadLevel(targetLevel);
    }

    public void NextWave(bool isNextLevelContinue = true)
    {
        int targetWave = WaveID + 1;
        if (targetWave >= m_levelData[m_levelID].m_waves.Count)
        {
            if (isNextLevelContinue)
            {
                targetWave = 0; // Next Level
                NextLevel();
            }
        }
        else
        {
            // StartWaveManager.Instance.ActivateStartWaveButton();

            WaveID = targetWave;
            EnemySpawn.Instance.BeginSpawning(m_levelData[m_levelID].m_waves[WaveID].m_duration);
            UpdatePlayerStats();
            EnemySpawn.Instance.enabled = true;

            // UIManager.Instance.m_defeatUI.Value.SetActive(false);
            // EnemySpawn.Instance.PoolAllAndStop();
            // BulletsManager.Instance.PoolAll();
            // BulletsManager.Instance.enabled = true;
            // EnemiesController.Instance.enabled = true;
            // CameraMover.Instance.enabled = true;
        }
    }

    public void UpdatePlayerStats()
    {
        UIManager.Instance.m_healthText.Value.text = $"{m_health}";
        UIManager.Instance.m_moneyText.Value.text = $"{m_money}";
        UIManager.Instance.m_waveText.Value.text = $"{m_waveID + 1}/{m_maxWave}";
    }

    void OnDefeat()
    {
        EnemiesController.Instance.enabled = false;
        CameraMover.Instance.enabled = false;
        BulletsManager.Instance.enabled = false;
        UIManager.Instance.m_defeatUI.Value.SetActive(true);
        TowerSlotMenuManager.Instance.HideMenu();
    }

    public void OnClick_RestartLevel()
    {
        Money = m_levelData[m_levelID].m_levelStartMoney;
        LoadLevel(m_levelID); // Start from first wave
    }

    public void Check_WaveIsCleared()
    {
        if (EnemiesController.Instance.m_enemyScripts.Count < 1 && EnemySpawn.Instance.IsSpawnCompleted())
        {
            Debug.Log("Wave cleared");
            UIManager.Instance.m_defeatUI.Value.SetActive(false);
            EnemySpawn.Instance.PoolAllAndStop();
            EnemiesController.Instance.enabled = true;
            CameraMover.Instance.enabled = true;
            NextWave();
        }
    }
}
