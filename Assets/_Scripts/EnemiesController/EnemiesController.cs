﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesController : MonoBehaviour
{
    #region Singleton Init
    private static EnemiesController _instance;

    void Awake() // Init in order
    {
        if (_instance == null)
            Init();
        else if (_instance != this)
        {
            Debug.Log($"Destroying {gameObject.name}, caused by one singleton instance");
            Destroy(gameObject);
        }
    }

    public static EnemiesController Instance // Init not in order
    {
        get
        {
            if (_instance == null)
                Init();
            return _instance;
        }
        private set => _instance = value;
    }

    static void Init() // Init script
    {
        _instance = FindObjectOfType<EnemiesController>();
        if (_instance != null)
            _instance.Initialize();
    }
    #endregion

    public List<EnemyScript> m_enemyScripts;
    public List<Transform> m_path;

    void Initialize()
    {
        // Init data here
        enabled = true;
    }

    private void FixedUpdate()
    {
        bool isMustRemove = false;
        foreach(var item in m_enemyScripts)
        {
            if (Vector3.Distance(item.m_targetPoint.position, item.transform.position) > 0.1f)
            {
                item.transform.position = Vector3.MoveTowards(item.transform.position, item.m_targetPoint.position, item.m_enemyData.m_speed * Time.deltaTime);
                item.m_pathPassed += item.m_enemyData.m_speed * Time.deltaTime;
            }
            else
            {
                int index = m_path.FindIndex((x) => x == item.m_targetPoint) + 1;
                if (index >= m_path.Count)
                {
                    // End
                    GameManager.Instance.Health -= item.m_enemyData.m_damage;
                    isMustRemove = true;
                    EnemySpawn.Instance.Pool(item.gameObject);
                }
                else
                    item.m_targetPoint = m_path[index];
            }
        }

        if (isMustRemove)
            m_enemyScripts.RemoveAll((x) => !x.gameObject.activeInHierarchy);
    }

    public void Register(EnemyScript enemyScript)
    {
        if (enemyScript != null)
            m_enemyScripts.Add(enemyScript);
        if (m_path != null && m_path.Count > 0)
            enemyScript.m_targetPoint = m_path[0];
        enemyScript.m_pathPassed = 0f;
    }

    public void PoolAll()
    {
        foreach (var item in m_enemyScripts)
            EnemySpawn.Instance.Pool(item.gameObject);
        m_enemyScripts.Clear();
    }

    public void Pool(EnemyScript enemyScript)
    {
        if (m_enemyScripts.Contains(enemyScript))
        {
            EnemySpawn.Instance.Pool(enemyScript.gameObject);
            m_enemyScripts.Remove(enemyScript);
        }
    }
}
