﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerManager : MonoBehaviour
{
    #region Singleton Init
    private static TowerManager _instance;

    void Awake() // Init in order
    {
        if (_instance == null)
            Init();
        else if (_instance != this)
        {
            Debug.Log($"Destroying {gameObject.name}, caused by one singleton instance");
            Destroy(gameObject);
        }
    }

    public static TowerManager Instance // Init not in order
    {
        get
        {
            if (_instance == null)
                Init();
            return _instance;
        }
        private set => _instance = value;
    }

    static void Init() // Init script
    {
        _instance = FindObjectOfType<TowerManager>();
        if (_instance != null)
            _instance.Initialize();
    }
    #endregion

    public List<TowerScript> m_towersReadyToShoot;

    void Initialize()
    {
        // Init data here
        enabled = true;
    }

    private void FixedUpdate()
    {
        m_towersReadyToShoot.RemoveAll((x) => x == null || !x.gameObject.activeInHierarchy);

        foreach (var item in m_towersReadyToShoot.ToArray())
        {
            EnemyScript targetEnemy = null;
            foreach (var enemy in EnemiesController.Instance.m_enemyScripts)
            {
                if (item.m_towerData.m_range >= Vector2.Distance(item.transform.position, enemy.transform.position))
                {
                    if (targetEnemy == null)
                        targetEnemy = enemy;
                    else if (enemy.m_pathPassed > targetEnemy.m_pathPassed)
                        targetEnemy = enemy;
                }
            }
            if (targetEnemy != null)
            {
                item.StartReload();
                m_towersReadyToShoot.Remove(item);
                BulletsManager.Instance.CreateBullet(
                    item.transform.position,
                    item.m_towerData.m_damage,
                    item.m_towerData.m_bulletSpeed,
                    item.m_towerData.m_splashRange,
                    (item.m_towerData.m_towerTypeEnum != TowerTypeEnum.Freeze) ? targetEnemy.transform.position : item.transform.position + Vector3.up,
                    item.m_towerData.m_bulletSprite,
                    item.m_towerData.m_spriteScale,
                    (item.m_towerData.m_towerTypeEnum == TowerTypeEnum.Army),
                    (item.m_towerData.m_towerTypeEnum == TowerTypeEnum.FastArrow || item.m_towerData.m_towerTypeEnum == TowerTypeEnum.Army) ? targetEnemy : null);
            }
        }
    }

    public void RegisterTowerReady(TowerScript towerScript)
    {
        m_towersReadyToShoot.Add(towerScript);
    }
}
