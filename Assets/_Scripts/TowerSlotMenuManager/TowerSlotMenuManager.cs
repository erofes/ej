﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TowerSlotMenuManager : MonoBehaviour
{
    #region Singleton Init
    private static TowerSlotMenuManager _instance;

    void Awake() // Init in order
    {
        if (_instance == null)
            Init();
        else if (_instance != this)
        {
            Debug.Log($"Destroying {gameObject.name}, caused by one singleton instance");
            Destroy(gameObject);
        }
    }

    public static TowerSlotMenuManager Instance // Init not in order
    {
        get
        {
            if (_instance == null)
                Init();
            return _instance;
        }
        private set => _instance = value;
    }

    static void Init() // Init script
    {
        _instance = FindObjectOfType<TowerSlotMenuManager>();
        if (_instance != null)
            _instance.Initialize();
    }
    #endregion

    public Transform m_towerSlotMenuTr;
    public Transform m_targetTr;
    public GameObject[] m_menuLayouts;
    public enum MenuLayoutEnum
    {
        Nothing = -1,
        FreeSlot = 0,
        AnyTower = 1,
        MaxLevelTower = 2
    }
    public MenuLayoutEnum m_currentMenuLayout = MenuLayoutEnum.Nothing;
    public float m_mouseUpSpeed = 0.15f; // How fast you should click to deselect selection
    public float m_mouseMagnitude = 20f; // Pixels to move that can be acceptable for deselection

    // For empty slot
    public Button m_fastArrowButton, m_splashButton, m_freezeButton, m_armyButton;
    // For attached tower
    public Button m_upgradeButton, m_sellButton;
    // Internal
    bool isClickOnButton;
    Coroutine hideOnMouseUpCoroutine;
    bool isPressed = false;
    Vector2 input;
    Vector2 delta;

    void Initialize()
    {
        m_fastArrowButton.onClick.AddListener(() =>
        {
            if (m_targetTr != null)
            {
                m_targetTr.GetComponent<TowerSlot>().TryActivateTower(TowerTypeEnum.FastArrow);
                AppendMenuToSlot(m_targetTr.GetComponent<TowerSlot>()); // Update layout
                GameManager.Instance.UpdatePlayerStats();
            }
        });
        m_splashButton.onClick.AddListener(() =>
        {
            if (m_targetTr != null)
            {
                m_targetTr.GetComponent<TowerSlot>().TryActivateTower(TowerTypeEnum.Splash);
                AppendMenuToSlot(m_targetTr.GetComponent<TowerSlot>()); // Update layout
                GameManager.Instance.UpdatePlayerStats();
            }
        });
        m_freezeButton.onClick.AddListener(() =>
        {
            if (m_targetTr != null)
            {
                m_targetTr.GetComponent<TowerSlot>().TryActivateTower(TowerTypeEnum.Freeze);
                AppendMenuToSlot(m_targetTr.GetComponent<TowerSlot>()); // Update layout
                GameManager.Instance.UpdatePlayerStats();
            }
        });
        m_armyButton.onClick.AddListener(() =>
        {
            if (m_targetTr != null)
            {
                m_targetTr.GetComponent<TowerSlot>().TryActivateTower(TowerTypeEnum.Army);
                AppendMenuToSlot(m_targetTr.GetComponent<TowerSlot>()); // Update layout
                GameManager.Instance.UpdatePlayerStats();
            }
        });
        m_upgradeButton.onClick.AddListener(() =>
        {
            if (m_targetTr != null)
            {
                m_targetTr.GetComponent<TowerSlot>().TryUpgradeCurrentTower();
                AppendMenuToSlot(m_targetTr.GetComponent<TowerSlot>()); // Update layout
                GameManager.Instance.UpdatePlayerStats();
            }
        });
        m_sellButton.onClick.AddListener(() =>
        {
            if (m_targetTr != null)
            {
                m_targetTr.GetComponent<TowerSlot>().TrySellTower();
                AppendMenuToSlot(m_targetTr.GetComponent<TowerSlot>()); // Update layout
                GameManager.Instance.UpdatePlayerStats();
            }
        });
        HideMenu();
        // Init data here
        enabled = true;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                isClickOnButton = true;
                return;
            }
            isPressed = true;
            input = Input.mousePosition;
            Vector2 origin = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D raycastHit2D = Physics2D.Raycast(origin, Vector2.zero);
            if (raycastHit2D.transform != null)
            {
                TowerSlot towerSlot = raycastHit2D.transform.GetComponent<TowerSlot>();
                if (towerSlot != null)
                    AppendMenuToSlot(towerSlot);
            }
            else
            {
                if (hideOnMouseUpCoroutine != null)
                    StopCoroutine(hideOnMouseUpCoroutine);
                hideOnMouseUpCoroutine = StartCoroutine(HideOnMouseUpCoroutine());
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            delta = new Vector2(input.x - Input.mousePosition.x, input.y - Input.mousePosition.y);
            isPressed = false;
        }
    }

    private void LateUpdate()
    {
        if (m_towerSlotMenuTr.gameObject.activeInHierarchy)
        {
            if (m_targetTr != null)
                m_towerSlotMenuTr.position = Camera.main.WorldToScreenPoint(m_targetTr.position);
            else
                HideMenu();
        }
    }

    IEnumerator HideOnMouseUpCoroutine()
    {
        float time = Time.time + m_mouseUpSpeed;
        while (Time.time < time)
        {
            if (!isPressed && delta.magnitude < m_mouseMagnitude)
            {
                HideMenu();
                yield break;
            }
            yield return new WaitForEndOfFrame();
        }
    }

    public void AppendMenuToSlot(TowerSlot towerSlot)
    {
        m_targetTr = towerSlot.transform;
        if (towerSlot.m_attachedTower != null)
        {
            // Show menu with tower inside
            if (towerSlot.IsMaxLevel())
                SetMenuLayout(MenuLayoutEnum.MaxLevelTower);
            else
                SetMenuLayout(MenuLayoutEnum.AnyTower);
        }
        else
        {
            // Show menu without tower
            SetMenuLayout(MenuLayoutEnum.FreeSlot);
        }
    }

    public void HideMenu()
    {
        SetMenuLayout(MenuLayoutEnum.Nothing);
    }

    public void SetMenuLayout(MenuLayoutEnum menuLayoutEnum)
    {
        m_currentMenuLayout = menuLayoutEnum;
        switch (menuLayoutEnum)
        {
            case MenuLayoutEnum.Nothing:
                m_towerSlotMenuTr.gameObject.SetActive(false);
                return;
            case MenuLayoutEnum.FreeSlot:
                break;
            case MenuLayoutEnum.AnyTower:
                break;
            case MenuLayoutEnum.MaxLevelTower:
                m_towerSlotMenuTr.gameObject.SetActive(true);
                for (int i = 0; i < m_menuLayouts.Length; i++)
                    m_menuLayouts[i].SetActive((int)MenuLayoutEnum.AnyTower == i);
                m_upgradeButton.gameObject.SetActive(false);
                return;
            default:
                break;
        }

        // All except "Nothing" & MaxLevelTower
        m_towerSlotMenuTr.gameObject.SetActive(true);
        for (int i = 0; i < m_menuLayouts.Length; i++)
            m_menuLayouts[i].SetActive((int)menuLayoutEnum == i);
        m_upgradeButton.gameObject.SetActive(true);
    }
}
