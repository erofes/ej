﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public float m_damage;
    public float m_speed;
    public float m_splashRange;
    public Vector3 m_targetPos;
    public EnemyScript m_targetEnemy; // Optioinally for auto aiming
    public bool m_isSoldier;
}
