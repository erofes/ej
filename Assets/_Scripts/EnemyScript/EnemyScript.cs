﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    public EnemyData m_enemyData;
    public Transform m_targetPoint;
    public float m_pathPassed;

    public void InitFromData(EnemyData enemyData)
    {
        m_enemyData = AnyClassCloner.CloneClass(enemyData);
    }

    public void ReciveDamage(float damage)
    {
        m_enemyData.m_health -= damage;
        if (m_enemyData.m_health <= 0f)
        {
            GameManager.Instance.Money += Random.Range(m_enemyData.m_moneyPerKill.x, m_enemyData.m_moneyPerKill.y);
            EnemiesController.Instance.Pool(this);
            GameManager.Instance.Check_WaveIsCleared();
        }
    }
}
