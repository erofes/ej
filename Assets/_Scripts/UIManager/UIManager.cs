﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    #region Singleton Init
    private static UIManager _instance;

    void Awake() // Init in order
    {
        if (_instance == null)
            Init();
        else if (_instance != this)
        {
            Debug.Log($"Destroying {gameObject.name}, caused by one singleton instance");
            Destroy(gameObject);
        }
    }

    public static UIManager Instance // Init not in order
    {
        get
        {
            if (_instance == null)
                Init();
            return _instance;
        }
        private set => _instance = value;
    }

    static void Init() // Init script
    {
        _instance = FindObjectOfType<UIManager>();
        if (_instance != null)
            _instance.Initialize();
    }
    #endregion

    public bool m_isShowCategories;

    [NaughtyAttributes.InfoBox("All panels", NaughtyAttributes.InfoBoxType.Normal, "m_isShowCategories")]
    public GameObjectString m_sceneUI;
    public GameObjectString m_defeatUI;

    [NaughtyAttributes.InfoBox("Top left UI Texts.", NaughtyAttributes.InfoBoxType.Normal, "m_isShowCategories")]
    public TmpTextString m_healthText;
    public TmpTextString m_moneyText;
    public TmpTextString m_waveText;

    [NaughtyAttributes.InfoBox("Defeat UI.", NaughtyAttributes.InfoBoxType.Normal, "m_isShowCategories")]
    public ButtonString m_defeatButton;
    public TmpTextString m_waveTimerText;

    void Initialize()
    {
        m_defeatButton.Value.onClick.AddListener(() => GameManager.Instance.OnClick_RestartLevel());
        // Init data here
        enabled = true;
    }
}
