﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartWaveManager : MonoBehaviour
{
    #region Singleton Init
    private static StartWaveManager _instance;

    void Awake() // Init in order
    {
        if (_instance == null)
            Init();
        else if (_instance != this)
        {
            Debug.Log($"Destroying {gameObject.name}, caused by one singleton instance");
            Destroy(gameObject);
        }
    }

    public static StartWaveManager Instance // Init not in order
    {
        get
        {
            if (_instance == null)
                Init();
            return _instance;
        }
        private set => _instance = value;
    }

    static void Init() // Init script
    {
        _instance = FindObjectOfType<StartWaveManager>();
        if (_instance != null)
            _instance.Initialize();
    }
    #endregion

    public Transform m_startWaveButton;
    public Transform m_targetTr;

    void Initialize()
    {
        m_startWaveButton.GetComponent<Button>().onClick.AddListener(() => OnClick_StartWave());
        // Init data here
        enabled = true;
    }

    private void LateUpdate()
    {
        if (m_startWaveButton.gameObject.activeInHierarchy && m_targetTr != null)
            m_startWaveButton.position = Camera.main.WorldToScreenPoint(m_targetTr.position);
    }

    public void OnClick_StartWave()
    {
        EnemySpawn.Instance.BeginSpawning(GameManager.Instance.m_levelData[GameManager.Instance.m_levelID].m_waves[GameManager.Instance.WaveID].m_duration);
        m_startWaveButton.gameObject.SetActive(false);
    }

    public void ActivateStartWaveButton()
    {
        m_startWaveButton.gameObject.SetActive(true);
    }
}
