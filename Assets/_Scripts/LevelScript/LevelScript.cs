﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelScript : MonoBehaviour
{
    public Transform m_startWaveButtonPos;
    public Transform m_spawnPointTr;
    public Transform m_pathTr;

    void Awake()
    {
        StartWaveManager.Instance.m_targetTr = m_startWaveButtonPos;
        EnemiesController.Instance.m_path.Clear();
        foreach (Transform tr in m_pathTr)
        {
            if (tr != m_pathTr)
                EnemiesController.Instance.m_path.Add(tr);
        }
        EnemySpawn.Instance.m_spawnPoint = m_spawnPointTr;
    }
}
