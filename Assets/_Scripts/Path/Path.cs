﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path : MonoBehaviour
{
    public List<Transform> m_points;

    [NaughtyAttributes.Button]
    void AppendPoints()
    {
        m_points.Clear();
        foreach(Transform tr in transform)
        {
            if (tr != transform)
                m_points.Add(tr);
        }
#if UNITY_EDITOR
        UnityEditor.EditorUtility.SetDirty(this);
#endif
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        if (m_points != null && m_points.Count > 1)
            for (int i = 0; i < m_points.Count - 1; i++)
            {
                if (m_points[i] != null && m_points[i + 1] != null)
                Gizmos.DrawLine(m_points[i].position, m_points[i + 1].position);
            }
    }
}
