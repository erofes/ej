﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerSlot : MonoBehaviour
{
    public List<GameObject> m_towers;
    public TowerTypeEnum m_currentTowerType;
    public TowerScript m_attachedTower;

    public void TryActivateTower(TowerTypeEnum towerTypeEnum)
    {
        if (GameManager.Instance.m_typeToTowerSODict[towerTypeEnum][0].m_towerData.m_price <= GameManager.Instance.Money)
        {
            GameManager.Instance.Money -= GameManager.Instance.m_typeToTowerSODict[towerTypeEnum][0].m_towerData.m_price;

            m_currentTowerType = towerTypeEnum;
            for (int i = 0; i < m_towers.Count; i++)
            {
                m_towers[i].SetActive(i == (int)m_currentTowerType);
                if (i == (int)m_currentTowerType)
                {
                    m_attachedTower = m_towers[i].GetComponent<TowerScript>();
                    m_attachedTower.InitFromData(GameManager.Instance.m_typeToTowerSODict[m_currentTowerType][0].m_towerData);
                    m_attachedTower.ChangeSprite(GameManager.Instance.m_typeToTowerSODict[m_currentTowerType][0].m_sprite);
                }
            }
        }
    }

    public void TryUpgradeCurrentTower()
    {
        if (!IsMaxLevel())
        {
            if (GameManager.Instance.m_typeToTowerSODict[m_currentTowerType][m_attachedTower.m_towerData.m_level].m_towerData.m_price <= GameManager.Instance.Money)
            {
                GameManager.Instance.Money -= GameManager.Instance.m_typeToTowerSODict[m_currentTowerType][m_attachedTower.m_towerData.m_level].m_towerData.m_price;

                m_attachedTower.ChangeSprite(GameManager.Instance.m_typeToTowerSODict[m_currentTowerType][m_attachedTower.m_towerData.m_level].m_sprite); // Should be first, before level change
                m_attachedTower.InitFromData(GameManager.Instance.m_typeToTowerSODict[m_currentTowerType][m_attachedTower.m_towerData.m_level].m_towerData); // m_level already + 1
            }
        }
    }

    public void TrySellTower()
    {
        if (m_attachedTower != null)
        {
            GameManager.Instance.Money += GameManager.Instance.m_typeToTowerSODict[m_currentTowerType][m_attachedTower.m_towerData.m_level - 1].m_towerData.m_price;

            for (int i = 0; i < m_towers.Count; i++)
            {
                m_towers[i].SetActive(false);
            }

            m_attachedTower = null;
        }
    }

    public bool IsMaxLevel()
    {
        int maxLevelOfTower = GameManager.Instance.m_typeToTowerSODict[m_currentTowerType].Count;
        return m_attachedTower.m_towerData.m_level >= maxLevelOfTower;
    }
}
