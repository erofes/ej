﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerScript : MonoBehaviour
{
    public TowerData m_towerData;
    // For shots
    public float m_reload;

    public void InitFromData(TowerData towerData)
    {
        m_towerData = AnyClassCloner.CloneClass(towerData);
        m_reload = Time.time; // Just to reset value
    }

    public void ChangeSprite(Sprite sprite)
    {
        GetComponent<SpriteRenderer>().sprite = sprite;
    }

    public void FixedUpdate()
    {
        // Fire On Ready
        if (m_reload <= Time.time)
        {
            TowerManager.Instance.RegisterTowerReady(this);
            enabled = false;
        }
    }

    public void StartReload()
    {
        m_reload = Time.time + m_towerData.m_shotRate;
        enabled = true;
    }
}
