﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Tower", menuName = "Database/Tower")]
public class TowerSO : ScriptableObject
{
    public Sprite m_sprite;
    public TowerData m_towerData;
}

[System.Serializable]
public class TowerData
{
    public TowerTypeEnum m_towerTypeEnum;
    public int m_level;
    public int m_price;
    public float m_range;
    public float m_damage;
    public float m_shotRate;
    public float m_splashRange;
    public float m_bulletSpeed; // For army it's army speed
    public Sprite m_bulletSprite;
    public float m_spriteScale;
}

public enum TowerTypeEnum
{
    FastArrow,
    Splash,
    Freeze,
    Army
}