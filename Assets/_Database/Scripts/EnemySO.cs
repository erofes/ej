﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Enemy", menuName = "Database/Enemy")]
public class EnemySO : ScriptableObject
{
    public Sprite m_sprite;
    public float m_enemySize;
    public EnemyData m_enemyData;
}

[System.Serializable]
public class EnemyData
{
    public float m_health;
    public float m_speed;
    public int m_damage;
    public Vector2Int m_moneyPerKill;
}