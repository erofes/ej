﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Wave", menuName = "Database/Wave")]
public class WaveSO : ScriptableObject
{
    public float m_duration;
    public WaveData m_waveData;
}

[System.Serializable]
public class WaveData
{
    public WaveEnemy[] waveEnemies;
}

[System.Serializable]
public class WaveEnemy
{
    public EnemySO m_enemySO;
    public int m_count;
    public float m_spawnRate;
}