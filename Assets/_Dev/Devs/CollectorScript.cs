﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// The main purpose of this script - to be able collect gameObject-s at startup even with SetActive(false) value. V2.5
public class CollectorScript : MonoBehaviour
{
    #region Singleton Init
    private static CollectorScript _instance;

    void Awake() // Init in order
    {
        if (_instance == null)
            Init();
        else if (_instance != this)
            Destroy(gameObject);
    }

    public static CollectorScript Instance // Init not in order
    {
        get
        {
            if (_instance == null)
                Init();
            return _instance;
        }
        private set { _instance = value; }
    }

    static void Init() // Init script
    {
        _instance = FindObjectOfType<CollectorScript>();
        if (_instance != null)
            _instance.Initialize();
    }
    #endregion

    List<GameObject> m_allGameObjects;
    Dictionary<string, GameObject> m_allGameObjectsDictionary;

    void Initialize()
    {
        m_allGameObjects = new List<GameObject>();
        m_allGameObjects.AddRange(Resources.FindObjectsOfTypeAll<GameObject>());
        m_allGameObjects.RemoveAll((x) => x.hideFlags != HideFlags.None);
        m_allGameObjectsDictionary = new Dictionary<string, GameObject>();
        foreach (var item in m_allGameObjects)
        {
            if (!m_allGameObjectsDictionary.ContainsKey(item.name))
                m_allGameObjectsDictionary.Add(item.name, item);
        }
        // Init data here
        enabled = true;
    }

    [NaughtyAttributes.Button("Re-initialize")]
    public void Reinitialize()
    {
        Initialize();
    }

    public T[] GetSceneGameObjects<T>(string _objectNameInScene)
    {
        List<T> collection = new List<T>();
        foreach (var item in m_allGameObjects)
            if (item.name.Equals(_objectNameInScene))
                collection.Add(item.GetComponent<T>());
        collection.RemoveAll(x => x == null); // Filter output
        return collection.ToArray();
    }

    public GameObject[] GetSceneGameObjectsNameContain(string _objectNameContainInScene)
    {
        List<GameObject> collection = new List<GameObject>();
        foreach (var item in m_allGameObjects)
            if (item.name.Contains(_objectNameContainInScene))
                collection.Add(item);
        collection.RemoveAll(x => x == null); // Filter output
        return collection.ToArray();
    }

    public GameObject GetSceneGameObject(string _objectNameInScene)
    {
        if (m_allGameObjectsDictionary.ContainsKey(_objectNameInScene))
            return m_allGameObjectsDictionary[_objectNameInScene];
        return null;
    }

    public T GetSceneGameObject<T>(string _objectNameInScene)
    {
        if (m_allGameObjectsDictionary.ContainsKey(_objectNameInScene))
            if (m_allGameObjectsDictionary[_objectNameInScene] != null)
                return m_allGameObjectsDictionary[_objectNameInScene].GetComponent<T>();
        return default;
    }

    public void InitProperty(ref GameObject _property, string _propertyName)
    {
        if (_property == null)
        {
            _property = GetSceneGameObject(_propertyName);
        }
    }

    public void InitProperty<T>(ref T _property, string _propertyName)
    {
        if (_property == null)
        {
            _property = GetSceneGameObject<T>(_propertyName);
        }
    }

    public void InitPropertyWhereRootIs<T>(ref T _property, string _propertyName, string _rootName)
    {
        if (_property == null)
        {
            Transform[] sceneInstances = GetSceneGameObjects<Transform>(_propertyName);
            foreach (var item in sceneInstances)
            {
                if (item.transform.root.name.Equals(_rootName))
                {
                    _property = item.GetComponent<T>();
                    return;
                }
            }
        }
    }

    public List<GameObject> FindAllWhereAnyParentIs(string _propertyName, string _parentName)
    {
        GameObject[] sceneInstances = GetSceneGameObjectsNameContain(_propertyName);
        List<GameObject> m_result =new List<GameObject>();
        if (string.IsNullOrEmpty(_parentName))
        {
            m_result.AddRange(sceneInstances);
            return m_result;
        }
        else
        {
            foreach (var item in sceneInstances)
            {
                Transform _parent = item.transform.parent;
                while (_parent != null)
                {
                    if (_parent.name.Contains(_parentName))
                    {
                        m_result.Add(item);
                        _parent = null;
                    }
                    else
                        _parent = _parent.parent;
                }
            }
            return m_result;
        }
    }
}
