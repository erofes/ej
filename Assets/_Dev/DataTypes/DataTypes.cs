﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
public static class DataTypes
{

}

[System.Serializable]
public class GameObjectString
{
    public string m_nameOfObject;
    public GameObject Value;

    public void SaveName()
    {
        if (Value != null)
            m_nameOfObject = Value.name;
        else if (string.IsNullOrEmpty(m_nameOfObject))
            m_nameOfObject = "";
    }
    public void LoadByName()
    {
        if (!string.IsNullOrEmpty(m_nameOfObject))
            CollectorScript.Instance.InitProperty(ref Value, m_nameOfObject);
    }
    public void LoadByChildName(string rootName)
    {
        if (!string.IsNullOrEmpty(m_nameOfObject))
            CollectorScript.Instance.InitPropertyWhereRootIs(ref Value, m_nameOfObject, rootName);
    }
}

[System.Serializable]
public class ButtonString
{
    public string m_nameOfObject;
    public Button Value;

    public void SaveName()
    {
        if (Value != null)
            m_nameOfObject = Value.name;
        else if (string.IsNullOrEmpty(m_nameOfObject))
            m_nameOfObject = "";
    }
    public void LoadByName()
    {
        if (!string.IsNullOrEmpty(m_nameOfObject))
            CollectorScript.Instance.InitProperty(ref Value, m_nameOfObject);
    }
    public void LoadByChildName(string rootName)
    {
        if (!string.IsNullOrEmpty(m_nameOfObject))
            CollectorScript.Instance.InitPropertyWhereRootIs(ref Value, m_nameOfObject, rootName);
    }
}

[System.Serializable]
public class ImageString
{
    public string m_nameOfObject;
    public Image Value;

    public void SaveName()
    {
        if (Value != null)
            m_nameOfObject = Value.name;
        else if (string.IsNullOrEmpty(m_nameOfObject))
            m_nameOfObject = "";
    }
    public void LoadByName()
    {
        if (!string.IsNullOrEmpty(m_nameOfObject))
            CollectorScript.Instance.InitProperty(ref Value, m_nameOfObject);
    }
    public void LoadByChildName(string rootName)
    {
        if (!string.IsNullOrEmpty(m_nameOfObject))
            CollectorScript.Instance.InitPropertyWhereRootIs(ref Value, m_nameOfObject, rootName);
    }
}

[System.Serializable]
public class TmpTextString
{
    public string m_nameOfObject;
    public TMPro.TextMeshProUGUI Value;

    public void SaveName()
    {
        if (Value != null)
            m_nameOfObject = Value.name;
        else if (string.IsNullOrEmpty(m_nameOfObject))
            m_nameOfObject = "";
    }
    public void LoadByName()
    {
        if (!string.IsNullOrEmpty(m_nameOfObject))
            CollectorScript.Instance.InitProperty(ref Value, m_nameOfObject);
    }
    public void LoadByChildName(string rootName)
    {
        if (!string.IsNullOrEmpty(m_nameOfObject))
            CollectorScript.Instance.InitPropertyWhereRootIs(ref Value, m_nameOfObject, rootName);
    }
}