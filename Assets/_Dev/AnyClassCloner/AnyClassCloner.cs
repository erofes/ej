﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AnyClassCloner
{
    public static T CloneClass<T>(T target) where T : new()
    {
        var bindingFlags = System.Reflection.BindingFlags.Instance |
                      System.Reflection.BindingFlags.NonPublic |
                      System.Reflection.BindingFlags.Public;
        var fieldValues = target.GetType()
                             .GetFields(bindingFlags)
                             .Select(field => field.GetValue(target))
                             .ToList();
        T clone = new T();
        var fields = clone.GetType()
                        .GetFields(bindingFlags);
        for (int i = 0; i < fields.Length; i++)
        {
            fields[i].SetValue(clone, fieldValues[i]);
        }
        return clone;
    }
}
