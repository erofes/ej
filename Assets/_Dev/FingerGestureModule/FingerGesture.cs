﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// V1.1
public class FingerGesture : MonoBehaviour
{
    #region Singleton Init
    private static FingerGesture _instance;

    void Awake() // Init in order
    {
        if (_instance == null)
            Init();
        else if (_instance != this)
            Destroy(gameObject);
    }

    public static FingerGesture Instance // Init not in order
    {
        get
        {
            if (_instance == null)
                Init();
            return _instance;
        }
        private set { _instance = value; }
    }

    static void Init() // Init script
    {
        _instance = FindObjectOfType<FingerGesture>();
        if (_instance != null)
            _instance.Initialize();
    }
    #endregion

    [Header("State of listener")]
    public bool m_isReadingGestureTwoFinger;
    public float m_diagonale;
    public float m_currentPercent;
    public float m_oldPercent;
    public float m_deltaPercent;
    [Header("Dev")]
    public bool m_isDebug;
    public GameObject m_objectForFingerPrefab;
    public Transform m_targetContainerTr;
    [System.Serializable]
    public class FingerInstance
    {
        public int m_fingerId;
        public Vector3 m_position;
        public GameObject m_fingerGO;
        public FingerInstance(int id, GameObject finger)
        {
            m_fingerId = id;
            m_fingerGO = finger;
        }
    }
    public List<FingerInstance> m_fingers;
    public bool m_isPermanentPosition;
    public Vector3 m_permanentPosition;

    void Initialize()
    {
        m_diagonale = Mathf.Sqrt(Screen.width * Screen.width + Screen.height * Screen.height);
#if !UNITY_EDITOR
        m_isDebug = false;
#endif
        // Init data here
        enabled = true;
    }

    private void Update()
    {
        if (m_isDebug)
        {
            if (m_isPermanentPosition && m_fingers.Find(x => x.m_fingerId == 1) == null)
            {
                if (m_objectForFingerPrefab != null)
                {
                    GameObject finger = Instantiate(m_objectForFingerPrefab, m_permanentPosition, Quaternion.identity, m_targetContainerTr);
                    finger.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = $"FID: 1";
                    m_fingers.Add(new FingerInstance(1, finger));
                    m_fingers.Find(x => x.m_fingerId == 1).m_position = m_permanentPosition;
                }
                else
                {
                    m_fingers.Add(new FingerInstance(1, null));
                    m_fingers.Find(x => x.m_fingerId == 1).m_position = m_permanentPosition;
                }
            }
            else if (!m_isPermanentPosition && m_fingers.Find(x => x.m_fingerId == 1) != null)
            {
                if (m_fingers.Find(x => x.m_fingerId == 1).m_fingerGO != null)
                    Destroy(m_fingers.Find(x => x.m_fingerId == 1).m_fingerGO);
                m_fingers.RemoveAll((x) => x.m_fingerId == 1);
            }

            if (Input.GetMouseButtonDown(0))
            {
                if (m_objectForFingerPrefab != null)
                {
                    GameObject finger = Instantiate(m_objectForFingerPrefab, Input.mousePosition, Quaternion.identity, m_targetContainerTr);
                    finger.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = $"FID: 0";
                    m_fingers.Add(new FingerInstance(0, finger));
                    m_fingers.Find(x => x.m_fingerId == 0).m_position = Input.mousePosition;
                }
                else
                {
                    m_fingers.Add(new FingerInstance(0, null));
                    m_fingers.Find(x => x.m_fingerId == 0).m_position = Input.mousePosition;
                }
                //Debug.Log(Input.mousePosition);
            }
            else if (Input.GetMouseButton(0))
            {
                m_fingers.Find(x => x.m_fingerId == 0).m_position = Input.mousePosition;
                if (m_fingers.Find(x => x.m_fingerId == 0).m_fingerGO != null)
                    m_fingers.Find(x => x.m_fingerId == 0).m_fingerGO.transform.position = Input.mousePosition;
                //Debug.Log(Input.mousePosition);
            }
            else if (Input.GetMouseButtonUp(0))
            {
                if (m_fingers.Find(x => x.m_fingerId == 0).m_fingerGO != null)
                    Destroy(m_fingers.Find(x => x.m_fingerId == 0).m_fingerGO);
                m_fingers.RemoveAll((x) => x.m_fingerId == 0);
            }
        }
        else
        {
            foreach (var item in Input.touches)
            {
                if (item.phase == TouchPhase.Began)
                {
                    if (m_objectForFingerPrefab != null)
                    {
                        GameObject finger = Instantiate(m_objectForFingerPrefab, item.position, Quaternion.identity, m_targetContainerTr);
                        finger.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = $"FID: {item.fingerId}";
                        m_fingers.Add(new FingerInstance(item.fingerId, finger));
                        m_fingers.Find(x => x.m_fingerId == item.fingerId).m_position = item.position;
                    }
                    else
                    {
                        m_fingers.Add(new FingerInstance(item.fingerId, null));
                        m_fingers.Find(x => x.m_fingerId == item.fingerId).m_position = item.position;
                    }
                }
                else if (item.phase == TouchPhase.Moved || item.phase == TouchPhase.Stationary)
                {
                    m_fingers.Find(x => x.m_fingerId == item.fingerId).m_position = item.position;
                    if (m_fingers.Find(x => x.m_fingerId == item.fingerId).m_fingerGO != null)
                        m_fingers.Find(x => x.m_fingerId == item.fingerId).m_fingerGO.transform.position = item.position;
                }
                else if (item.phase == TouchPhase.Canceled || item.phase == TouchPhase.Ended)
                {
                    if (m_fingers.Find(x => x.m_fingerId == item.fingerId).m_fingerGO != null)
                        Destroy(m_fingers.Find(x => x.m_fingerId == item.fingerId).m_fingerGO);
                    m_fingers.RemoveAll((x) => x.m_fingerId == item.fingerId);
                }
            }
        }
        // Process Gesture
        GestureTwoFingers();
    }

    void GestureTwoFingers()
    {
        if (m_fingers.Count > 1)
        {
            float distance = Vector3.Distance(m_fingers[0].m_position, m_fingers[1].m_position);
            m_oldPercent = m_currentPercent;
            m_currentPercent = distance / m_diagonale;
            if (!m_isReadingGestureTwoFinger)
                m_oldPercent = m_currentPercent; // Remember current as is
            m_deltaPercent = m_currentPercent - m_oldPercent;
            //Debug.Log($"Bool:{m_isReadingGestureTwoFinger} Distance:{distance} cur:{m_currentPercent} old:{m_oldPercent} delt:{m_deltaPercent}");
            if (!m_isReadingGestureTwoFinger)
                m_isReadingGestureTwoFinger = true;
        }
        else if (m_isReadingGestureTwoFinger)
            m_isReadingGestureTwoFinger = false;
    }
}
